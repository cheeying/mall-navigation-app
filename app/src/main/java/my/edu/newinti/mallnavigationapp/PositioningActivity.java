package my.edu.newinti.mallnavigationapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collection;

import es.situm.sdk.SitumSdk;
import es.situm.sdk.error.Error;
import es.situm.sdk.model.cartography.Building;
import es.situm.sdk.model.cartography.Floor;
import es.situm.sdk.utils.Handler;

public class PositioningActivity extends GetBuildingActivity{

    private static final String TAG = PositioningActivity.class.getSimpleName();

    private String buildingId;
    private ImageView imageViewLevel;

    private Building selectedBuilding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        buildingId = getBuildingFromIntent().getIdentifier();


        //Get all the buildings of the account
        SitumSdk.communicationManager().fetchBuildings(new Handler<Collection<Building>>() {
            @Override
            public void onSuccess(Collection<Building> buildings) {
                Log.d(TAG, "onSuccess: Your buildings: ");
                for (Building building : buildings) {
                    Log.i(TAG, "onSuccess: " + building.getIdentifier() + " - " + building.getName());

                    if (buildingId.equals(building.getIdentifier())) {
                        selectedBuilding = building;
                    }
                }

                if (buildings.isEmpty()) {
                    Log.e(TAG, "onSuccess: you have no buildings. Create one in the Dashboard");
                    return;
                }

                displayFloorImage();
            }

            @Override
            public void onFailure(Error error) {
                Log.e(TAG, "onFailure:" + error);
            }
        });
    }

    /**
     * Display the floor image
     */
    private void displayFloorImage() {
        //Get all the building floors
        SitumSdk.communicationManager().fetchFloorsFromBuilding(selectedBuilding.getIdentifier(), new Handler<Collection<Floor>>() {
            @Override
            public void onSuccess(Collection<Floor> floors) {
                Log.i(TAG, "onSuccess: received levels: " + floors.size());
                Floor floor = new ArrayList<>(floors).get(0);
                //Get the floor image bitmap
                SitumSdk.communicationManager().fetchMapFromFloor(floor, new Handler<Bitmap>() {
                    @Override
                    public void onSuccess(Bitmap bitmap) {
                        imageViewLevel.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onFailure(Error error) {
                        Log.e(TAG, "onFailure: fetching floor map: " + error);
                    }
                });
            }

            @Override
            public void onFailure(Error error) {
                Log.e(TAG, "onFailure: fetching floors: " + error);
            }
        });
    }
}
