package my.edu.newinti.mallnavigationapp;

import androidx.appcompat.app.AppCompatActivity;
import es.situm.sdk.SitumSdk;


import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SitumSdk.init(this);

    }
}
